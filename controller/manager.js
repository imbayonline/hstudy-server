const e = require("express");
const connection = require("../connection");

/*
 *  s1: database
 *  s2: your data
 */
function checkStudentAttendance(s1, s2) {
  if (s2.length == 0) {
    s1.forEach((element) => {
      element["isDone"] = false;
    });
  }
  s1.map((e) => {
    if (s2.some((ele) => ele.vid === e.id)) {
      e["isDone"] = true;
    } else {
      e["isDone"] = false;
    }
  });

  return s1;
}

module.exports.enrollCourse = (req, res, next) => {
  connection.query(`SELECT * FROM StudentCourse WHERE sid = ${req.query.user} AND cid = ${req.params.courseId};`,(error,response) => {
    if(error) return res.status(400).json({success: false, error})
    else if(response.length > 0) return res.status(200).json({success: true, message: "Already enrolled"})
    else connection.query(
      `INSERT INTO StudentCourse values (${req.query.user},${req.params.courseId},"Standard");`,
      (error, response) => {
        if (error) return res.status(400).json({ success: false, error });
        else return res.status(201).json({ success: true, message: "Enroll course successfully" });
      }
    );
  })
 
};

module.exports.attendVideo = (req, res, next) => {
  connection.query(`SELECT * FROM StudentVideo WHERE sid = ${req.query.user} AND vid = ${req.params.videoId};`,(error,response) => {
    if(error) return res.status(400).json({success: false, error})
    else if(response.length > 0) return res.status(200).json({success: true, message: "Already watched"})
    else connection.query(
      `INSERT INTO StudentVideo values (${req.query.user},${req.params.videoId},"Done");`,
      (error, response) => {
        if (error) return res.status(400).json({ success: false, error });
        else return res.status(201).json({ success: true, message: "Watch successfully" });
      }
    );
  })
};

module.exports.enrollmentAttendance = (req, res, next) => {
  connection.query(
    `SELECT * FROM VideoCourse WHERE course = ${req.params.courseId};`,
    (error, videos) => {
      if (error) return res.status(400).json({ success: false, error });

      connection.query(
        `SELECT
            StudentVideo.vid,
            StudentVideo.status,
            VideoCourse.videoName,
            VideoCourse.videoUrl
            FROM
            StudentVideo
                INNER JOIN
            VideoCourse ON StudentVideo.vid = VideoCourse.id
            WHERE
            VideoCourse.course =${req.params.courseId}
                AND StudentVideo.sid = ${req.query.user};`,
        (err, student_videos) => {
          if (err) return res.status(400).json({ success: false, error });

          return res.status(200).json({
            remain: videos.length - student_videos.length,
            undone:
              student_videos.length === 0
                ? false
                : videos.length - student_videos.length !== 0
                ? true
                : false,
            data: checkStudentAttendance(videos, student_videos),
          });
        }
      );
    }
  );
};
