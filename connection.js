const mysql = require("mysql");

const connection = mysql.createConnection({
  host: "database-1.cl5vxfb8wekq.ap-east-1.rds.amazonaws.com",
  port: 3306,
  user: "store_admin",
  password: "qwe123!@#",
  database: "store_db",
  multipleStatements: true,
  charset: 'utf8mb4'
});

connection.connect(err => {
  if (err) console.log(err.stack);
  console.log("Connected as " + connection.threadId);
});

module.exports = connection;
