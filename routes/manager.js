const router = require("express").Router();
const manager = require("../controller/manager");


router.get("/attendance/:courseId",manager.enrollmentAttendance);
router.post("/enroll/:courseId",manager.enrollCourse);
router.post("/watch/:videoId",manager.attendVideo);

module.exports = router;