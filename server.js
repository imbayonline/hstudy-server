const express = require("express");
const app = express();
const cors = require("cors");
const os = require("os");
const connection = require("./connection");

require("dotenv").config();

const PORT = process.env.PORT || 8088;

const courses = require("./routes/courses");
const providers = require("./routes/providers");
const authors = require("./routes/authors");
const videos = require("./routes/videos");
const students = require("./routes/students");
const manager = require("./routes/manager");

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cors());

app.use("/uploads", express.static("uploads"));
app.use("/", express.static(__dirname));

app.use("/courses", courses);
app.use("/providers", providers);
app.use("/authors", authors);
app.use("/videos", videos);
app.use("/students", students);
app.use("/manager", manager);
app.use("/sponsors", (req, res) => {
  let queryString = "";
  if (req.query.limit === undefined) {
    queryString = `SELECT * FROM Sponsor`;
  } else {
    queryString = `SELECT * FROM Sponsor LIMIT ${req.query.limit}`;
  }
  connection.query(queryString, (error, documents) => {
    if (error) return res.status(400).json({ success: false, error });
    return res.status(200).json({ success: true, data: documents });
  });
});

app.listen(PORT, () => {
  console.log(`Listening on port ${PORT} on ${os.platform()}`);
});
