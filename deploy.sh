#!/bin/bash
BOEC_VERSION=$(cat version.txt)
echo "Previous Version: " $BOEC_VERSION
docker kill hstudy_server$BOEC_VERSION
git pull
BOEC_VERSION=$(cat version.txt)
echo "Current Version: " $BOEC_VERSION
docker build -t quochung5c/hstudy_server:$BOEC_VERSION .
docker run -d -p 8088:8088 --name hstudy_server$BOEC_VERSION quochung5c/hstudy_server:$BOEC_VERSION

